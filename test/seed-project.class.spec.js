import SeedProject from '../src/seed-project.class';

describe('SeedProject', () => {

    it('is defined', () => {
        expect(SeedProject).toBeDefined();
    });

    /** @type {SeedProject} */
    let seedProject;

    beforeEach(() => {
        seedProject = new SeedProject();
    });

    describe('showGreeting()', () => {

        it('adds the greeting to the page', () => {
            seedProject.showGreeting('This is a test.');
            let greetingText = document.body.getElementsByTagName('p')[0].innerText;
            expect(greetingText).toEqual('This is a test.');
        });

    });

});