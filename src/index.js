import SeedProject from './seed-project.class';

(() => {
    let seedProject = new SeedProject();
    seedProject.showGreeting('Hello seed project!');
})();
