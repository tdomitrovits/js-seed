export default class SeedProject {
    constructor() {
    }

    /**
     * Shows a greeting message
     * @param {String} message 
     */
    showGreeting(message) {
        let greeting = document.createElement('p');
        greeting.innerText = message;
        document.body.appendChild(greeting);
    }
}