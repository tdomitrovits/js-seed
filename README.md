# Getting Started
Execute `npm run build` to build the deployable artifacts in the dist folder.
Execute `npm run coverage` to run the test suite and generate a code coverage report.
Execute `npm run dev` to launch a web browser with hot reloading for development.
Execute `npm run test` to run tests in watch mode for test development.
